
/**
  * @brief Tx Transfer completed callbacks
  * @param huart: uart handle
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	UNUSED(huart);

	// Card Dispenser
	else if (huart->Instance==USART3)
	{
		// Notify to GCM Library the end of trasnmission, to let put in Reception Mode
		gcm_Outcoming_Data();
	}
}


/**
  * @brief Rx Transfer completed callbacks
  * @param huart: uart handle
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	UNUSED(huart);
	
	// GCM SmartCard Data
	else if(huart->Instance==USART3)
	{
		gcm_Incomming_Data(*Rx_GCM_data);
		
		// Re-Enable Reception Interrupts
		HAL_UART_Receive_IT(&huart3, (uint8_t *)Rx_GCM_data, 1);
	}

}




void gcm()
{
	// Create and Init Structure
	struct gcm_t *gcm;
	gcm = gcm_init(&Rx_GCM_data);

	// Make a card Reset
	gcm_reset(gcm->atr);

	// Chek if card Response is OK
	if (check_gcm_atr(gcm))
	{
		// Read all memory
		gcm_dump_memory(gcm, &Rx_GCM_data);


		// Send the PIN of 3 secctions.
		gcm_auth(gcm, 0, 0x00, 0x00, 0x00, 0x00, &Rx_GCM_data);
		gcm_auth(gcm, 1, 0x11, 0x11, 0x11, 0x11, &Rx_GCM_data);
		gcm_auth(gcm, 2, 0x22, 0x22, 0x22, 0x22, &Rx_GCM_data);

		// Check if was Correct Authentificate with 3 PINS
		if (gcm->auth0 && gcm->auth1 && gcm->auth2)
		{
			// Write a value in Application Area 1
			char values[5];
			values[0] = 0x00; values[1] = 0x00;	values[2] = 0x00; values[3] = 0x00;
			gcm_write4Bytes(0x08, &values, &Rx_GCM_data);

			// Read to Verify the correct write.
			gcm_dump_memory(gcm, &Rx_GCM_data);
			
			
			// Change PIN of AREA 2
			int res = gcm_Change_PIN(gcm, 2, 0x33, 0x33, 0x33, 0x33, &Rx_GCM_data);
			if (res==1)
			{
				// PIN CHANGE OK
				//res=0;
			}
			else
			{
				// PIN Change ERROR
			}
		}
	}

	// Free Memory
	gcm_free(gcm);

}