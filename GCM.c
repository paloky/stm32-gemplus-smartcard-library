/*
 * GCM.c
 *
 *  Created on: 6 mar. 2019
 *      Author: Sergi Gomis
 *      e-mail:  paloky3@gmail.com
 */

#include <stdint.h>
#include <stdlib.h>
#include "GCM.h"
#include "string.h"


char InBuffer[10];
uint8_t InIndex=0;

bool OutData=false;


// Private Prototipes
void set_uart();
uint8_t Read_4Bytes(uint8_t address, uint8_t *mm, uint8_t *Rx_GCM_data);
uint8_t Write4Bytes(uint8_t address, uint8_t *values, uint8_t *Rx_GCM_data);




/**
 * Allocate Memori and Init Variable States
 * [param] *Rx_GCM_data Pointer of UART Receiver buffer
 *
 * [return] GCM Inititated Struct
 */
struct gcm_t* gcm_init(uint8_t *Rx_GCM_data)
{
	struct gcm_t *gcm;

	gcm = malloc(sizeof(struct gcm_t));
	gcm->atr = malloc(4);
	gcm->main_memory = malloc(256);
	gcm->auth0=0;
	gcm->auth1=0;
	gcm->auth2=0;

	// Config Uart
	//set_uart();

	// Enable UART Interrupts
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t*)Rx_GCM_data, 1);

	return(gcm);
}


/**
 * Free Alocated Memory
 * [param] *gcm Pointer of GCM Struct
 */
void gcm_free(struct gcm_t *gcm)
{
	free(gcm->main_memory);
	free(gcm->atr);
	free(gcm);
}


/**
 * Put UART Received Data to Incomming Data Buffer
 * [param] value UART Data received
 */
void gcm_Incomming_Data(uint8_t value)
{
	InBuffer[InIndex] = value;
	InIndex++;
}


/**
 * Notify the end of UART Trasnmission
 */
void gcm_Outcoming_Data()
{
	OutData=true;
}


/**
 * Make a card Reset, and Read the Card Response Code
 * [param] *atr Pointer of Reset Code Attribute
 */
void gcm_reset(uint8_t *atr)
{
	InIndex=0;

	set_rst_0;
	HAL_Delay(100);
	HAL_Delay(25);

	set_rst_1;
	HAL_Delay(100);

	// Read Card Response Codes
	for (int i=0; i<4; i++)
	{
		*(atr+i) = InBuffer[i];
	}

}



/**
 * Check if Received Reset Codes are valid
 *
 * [return] 1 = Check OK
 */
uint8_t check_gcm_atr(struct gcm_t *gcm)
{
	uint8_t atr[4] = {0x3B, 0x02, 0x53, 0x01};

	if (memcmp(gcm->atr, &atr, 4))
		return(0);
	else
		return(1);
}



/**
 * Read all memory map
 * [param] *gcm Struct of gcm
 * [param] *Rx_GCM_data Pointer of UART Receiver buffer
 */
void gcm_dump_memory(struct gcm_t *gcm, uint8_t *Rx_GCM_data)
{
	// Read from 4 to 4 bytes
	for (int address=0; address<0x3F; address++)
	{
		Read_4Bytes(address, gcm->main_memory, &Rx_GCM_data);
	}
}



/**
 * Send PIN to Card
 * @param *gcm Struct of gcm
 * [param] pin_number PIN of Access Block (0, 1 o 2).
 * [param] pin1
 * [param] pin2
 * [param] pin3
 * [param] pin4
 * [param] *Rx_GCM_data Pointer of UART Receiver buffer
 */
void gcm_auth(struct gcm_t *gcm, uint8_t pin_number, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3, const uint8_t pin4, uint8_t *Rx_GCM_data)
{

	if (pin_number!=0 && pin_number!=1 && pin_number!=2)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}  // No valid PIN

	// THIS PINS VALUES ARE FORBIDEN FOR MANUFACTURER
	if (pin1==0x00 && pin2==0x00 && pin3==0x00 && pin4==0x00)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}
	else if (pin1==0x80 && pin2==0x00 && pin3==0x00 && pin4==0x00)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}
	else if (pin1==0x7F && pin2==0xFF && pin3==0xFF && pin4==0xFF)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}
	else if (pin1==0xFF && pin2==0xFF && pin3==0xFF && pin4==0xFF)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}


	// Check the number of remain intents
	Read_4Bytes(0x07, gcm->main_memory, &Rx_GCM_data);
	// If only remain ONE try, return Error, to prevent block the card.
	uint8_t num_of_intents = *(gcm->main_memory+0x1C);
	if ( num_of_intents >= 0b11100000)
	{
		if (pin_number==0) gcm->auth0=0;
		else if (pin_number==1) gcm->auth1=0;
		else if (pin_number==2) gcm->auth2=0;
		return;
	}


	uint8_t cmd[10];
	cmd[0] = 0x80;
	cmd[1] = 0x20;  // VERIFY PIN COMMAND
	cmd[2] = 0x00;
	cmd[3] = 0x07;  // VERIFY PIN 0
	cmd[4] = 0x04;
	cmd[5] = pin4;
	cmd[6] = pin3;
	cmd[7] = pin2;
	cmd[8] = pin1;
	cmd[9] = '\0';

	if (pin_number==0) 	cmd[3] = 0x07;  // VERIFY PIN 0
	else if (pin_number==1) cmd[3] = 0x39;  // VERIFY PIN 1
	else if (pin_number==2) cmd[3] = 0x3B;  // VERIFY PIN 2


	// Send Command
	HAL_HalfDuplex_EnableTransmitter(&gcm_uart);
	InIndex=0;
	OutData=false;
	HAL_UART_Transmit_IT(&gcm_uart, &cmd[0], 5);
	while(OutData==false);	OutData=false;

	// Set in Reception Mode
	HAL_HalfDuplex_EnableReceiver(&gcm_uart);
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t *)Rx_GCM_data, 1);
	HAL_Delay(10);


	// Send PIN
	HAL_HalfDuplex_EnableTransmitter(&gcm_uart);
	InIndex=0;
	OutData=false;
	HAL_UART_Transmit_IT(&gcm_uart, &cmd[5], 4);
	while(OutData==false);	OutData=false;
	// Set in Recetption Mode to Read the Card Response
	HAL_HalfDuplex_EnableReceiver(&gcm_uart);
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t *)Rx_GCM_data, 1);

	HAL_Delay(50);


	if (InIndex>=2)
	{
		// Check the Status response
		if (InBuffer[0]==0x90)
		{
			// Valid PIN
			if (pin_number==0) gcm->auth0=1;
			else if (pin_number==1) gcm->auth1=1;
			else if (pin_number==2) gcm->auth2=1;
			return;
		}
		else
		{
			if (pin_number==0) gcm->auth0=0;
			else if (pin_number==1) gcm->auth1=0;
			else if (pin_number==2) gcm->auth2=0;
		}
	}


	// PIN NOT VALID
	if (pin_number==0) gcm->auth0=0;
	else if (pin_number==1) gcm->auth1=0;
	else if (pin_number==2) gcm->auth2=0;

}



/**
 * Change The PIN of Area
 * @param *gcm Struct of gcm
 * @param pin_number Is the Area to change the PIN
 * @param pin1
 * @param pin2
 * @param pin3
 * @param pin4
 * @param *Rx_GCM_data Pointer of UART Receiver buffer
 *
 * @return 1 = Change OK
 */
uint8_t gcm_Change_PIN(struct gcm_t *gcm, uint8_t pin_number, uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4, uint8_t *Rx_GCM_data)
{

	if (pin_number!=0 && pin_number!=1 && pin_number!=2)
	{
		return 0;
	}  // No valid PIN

	// THIS PINS VALUES ARE FORBIDEN FOR MANUFACTURER
	if (pin1==0x00 && pin2==0x00 && pin3==0x00 && pin4==0x00) return 0;
	else if (pin1==0x80 && pin2==0x00 && pin3==0x00 && pin4==0x00) return 0;
	else if (pin1==0x7F && pin2==0xFF && pin3==0xFF && pin4==0xFF) return 0;
	else if (pin1==0xFF && pin2==0xFF && pin3==0xFF && pin4==0xFF)return 0;

	char new_pin[6];
	new_pin[0]=pin4;
	new_pin[1]=pin3;
	new_pin[2]=pin2;
	new_pin[3]=pin1;
	new_pin[4]='0';

	// Select the correct Address in function of PIN Area
	uint8_t address;
	if (pin_number==0) address = 0x06;
	else if(pin_number==1) address = 0x38;
	else if (pin_number==2) address = 0x3A;


	if (address!=0x00)
	{
		// Write the NEW PIN
		if (gcm_write4Bytes(address, &new_pin, &Rx_GCM_data)) return 0;
		else return 1;
	}

	// Return ERROR
	return 0;
}




/**
 * Write 4 bytes to card Memori
 * [param] address Address base to Write
 * [param] *values Pointer of Values to Write
 * [param] *Rx_GCM_data Pointer of UART Receiver buffer
 *
 * [return] 1 = Error
 */
uint8_t gcm_write4Bytes(uint8_t address, uint8_t *values, uint8_t *Rx_GCM_data)
{

	uint8_t cmd[6];
	cmd[0] = 0x80;
	cmd[1] = 0xDE;  // Update Command
	cmd[2] = 0x00;
	cmd[3] = address;
	cmd[4] = 0x04;
	cmd[5] = '\0';

	// Send Command
	HAL_HalfDuplex_EnableTransmitter(&gcm_uart);
	InIndex=0;
	OutData=false;
	HAL_UART_Transmit_IT(&gcm_uart, &cmd[0], 5);
	while(OutData==false);	OutData=false;
	// Set in Reception Mode
	HAL_HalfDuplex_EnableReceiver(&gcm_uart);
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t *)Rx_GCM_data, 1);
	HAL_Delay(5);

	// Check if card respons with Command Sended
	if (InIndex>=1)
	{
		// Received Error Code.
		if (InBuffer[0]!=0xDE) return 1;
	}


	// Send 4 bytes to Write
	HAL_HalfDuplex_EnableTransmitter(&gcm_uart);
	InIndex=0;
	OutData=false;
	HAL_UART_Transmit_IT(&gcm_uart, values, 4);
	while(OutData==false);	OutData=false;
	// Set in reception Mode
	HAL_HalfDuplex_EnableReceiver(&gcm_uart);
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t *)Rx_GCM_data, 1);

	HAL_Delay(50);

	if (InIndex>=2)
	{
		// Check the Status Response. (0x90 = Transaction OK)
		if (InBuffer[0]==0x90) return 0;
		else return 1;
	}
	else return 1;

}






//***********************************************
//  PRIVATE FUNCTIONS
//***********************************************



/*
void set_uart()
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

    __HAL_RCC_USART3_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    //USART3 GPIO Configuration
	//PB10     ------> USART3_TX

    GPIO_InitStruct.Pin = UART_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	//GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_USART3;
	HAL_GPIO_Init(UART_PORT, &GPIO_InitStruct);


	huart3.Instance = USART3;
	huart3.Init.BaudRate = 9600;
	huart3.Init.WordLength = UART_WORDLENGTH_8B;
	huart3.Init.StopBits = UART_STOPBITS_1;
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_HalfDuplex_Init(&huart3) != HAL_OK)
	{
	Error_Handler();
	}
}
*/


/**
 * Read 4 Bytes from Memory
 * [param] address Adress base to write
 * [param] *mm Pointer to gcm->main_memory
 * [param] *Rx_GCM_data Pointer of UART Receiver buffer
 *
 * [return] 1 = Error
 */
uint8_t Read_4Bytes(uint8_t address, uint8_t *mm, uint8_t *Rx_GCM_data)
{

	uint8_t cmd[6];
	cmd[0] = 0x80;
	cmd[1] = 0xBE;	// READ Command
	cmd[2] = 0x00;
	cmd[3] = address;
	cmd[4] = 0x04;
	cmd[5] = '\0';

	// Send Command
	HAL_HalfDuplex_EnableTransmitter(&gcm_uart);
	InIndex=0;
	OutData=false;
	HAL_UART_Transmit_IT(&gcm_uart, &cmd[0], 5);
	while(OutData==false);	OutData=false;


	// Set reception Mode
	HAL_HalfDuplex_EnableReceiver(&gcm_uart);
	HAL_UART_Receive_IT(&gcm_uart, (uint8_t *)Rx_GCM_data, 1);

	HAL_Delay(50);

	if (InIndex>=6)
	{
		// Chech the Status Response of Card (SW1 & SW2)
		if (InBuffer[5]==0x90)
		{
			// InBuffer[0] = 0xBE
			// Read the returned values in Invers Order
			*(mm+(address*4)) = InBuffer[4];
			*(mm+(address*4)+1) = InBuffer[3];
			*(mm+(address*4)+2) = InBuffer[2];
			*(mm+(address*4)+3) = InBuffer[1];

			return 0;
		}

	}

	return 1;
}




