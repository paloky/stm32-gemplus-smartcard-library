/*
 * GCM.h
 *
 *  Created on: 6 mar. 2019
 *      Author: Sergi Gomis
 *      e-mail:  paloky3@gmail.com
 *
 */

#ifndef GCM_H_
#define GCM_H_

#include "main.h"
#include <sys/types.h>
#include <stdbool.h>
#include "usart.h"

// DEFINE THE HARDWARE PINOUTS
#define UART_PORT GPIOB
#define UART_PIN GPIO_PIN_10

#define card_detect HAL_GPIO_ReadPin(T_DT_GPIO_Port, T_DT_Pin)

#define set_ck_1 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_SET);
#define set_ck_0 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_RESET);

#define set_rst_1 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_SET);
#define set_rst_0 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_RESET);


// DEFINE THE UART ARE USED
#define gcm_uart huart3

// SERIAL PORT CONFIGURATION: (With CubeMX)
//-----------------------------------------
// Baud Rate = 9600bps
// Num_of_bits = 9 bits (Including parity)
// Parity = EVEN
// Stop_bits = 2 bits




struct gcm_t
{
	uint8_t *atr;
	uint8_t *main_memory;
	//uint8_t card_present;
	uint8_t auth0;
	uint8_t auth1;
	uint8_t auth2;
};



struct gcm_t* gcm_init(uint8_t *Rx_data);
void gcm_free(struct gcm_t *gcm);
void gcm_Incomming_Data(uint8_t value);
void gcm_Outcoming_Data();
void gcm_reset(uint8_t *atr);
uint8_t check_gcm_atr(struct gcm_t *gcm);
void gcm_dump_memory(struct gcm_t *gcm, uint8_t *Rx_GCM_data);
void gcm_auth(struct gcm_t *gcm, uint8_t pin_number, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3, const uint8_t pin4, uint8_t *Rx_GCM_data);
uint8_t gcm_Change_PIN(struct gcm_t *gcm, uint8_t pin_number, uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4, uint8_t *Rx_GCM_data);
uint8_t gcm_write4Bytes(uint8_t address, uint8_t *values, uint8_t *Rx_GCM_data);


#endif /* GCM_H_ */
